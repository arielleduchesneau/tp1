document.addEventListener("DOMContentLoaded", function (event) {

    document.getElementById("connexion-lien").addEventListener("click", function (e) {

        e.preventDefault();
        document.querySelector("#connexion-modale").classList.add("open")

    })


    document.getElementById("fermer-connexion-modale").addEventListener("click", function (e) {

        e.preventDefault();
        document.querySelector("#connexion-modale").classList.remove("open")

    })


// fleche remontante
    document.getElementById('arrow').addEventListener('click', function (event) {
        var intervalId = 0;

        function scrollStep() {
            if (window.pageYOffset === 0) {
                clearInterval()
            }
            window.scroll(0, window.pageYOffset - 50)
        }

        function scrollToTop() {
            intervalId = setInterval(scrollStep, 16.66);
        }

        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        })
    });


});